﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace KakaoExamples
{
    class Program
    {
        // 국문(영어, 한글, 일본어 등)과 숫자, 언더 바('_')의 조합. 숫자로 시작할 수 없음
        public const string REGEX_VALUE_NAME = @"[^\d\W][\w]*";

        // 예약된 이름
        public const string REGEX_RESERVED_VALUE_NAME = @"@[^\d\W][\w]*";

        // Json path 규칙
        public const string REGEX_JSON_PATH = @"\$[^${}]*";

        // Group 1 = Value path, 2 = Value name, 3 = Json path
        public const string REGEX_REFERENCE_VALUE = @"{((" + REGEX_RESERVED_VALUE_NAME + "|" + REGEX_VALUE_NAME + @")(" + REGEX_JSON_PATH + "))}";

        // Group 1 = Text value
        public const string REGEX_TEXT_VALUE = @"""([^""\r\n\b\v]*)""";

        public const string REGEX_VALUE_PART_NAME = @"(?<=(?<left>[.@{]))(?<name>[^${}@]+?)(?=(?<right>[.\[$}]))";

        static void Main(string[] args)
        {
            //Method1();
            //Method2();

            //var r = solution3(new int[] { 95, 90, 99, 99, 80, 99 }, new int[] { 1, 1, 1, 1, 1, 1 });
            //foreach (int i in r)
            //{
            //    Console.WriteLine(i);
            //}


            //Console.WriteLine(createMaxNum(new int[] { 12, 121 }, 12121));
            //Console.WriteLine(string.Join(",", stockPrice(new int[] { 1, 2, 3, 2, 3 })));
            //Console.WriteLine(string.Join(",",
            //    workingProcess(
            //        new int[] { 93, 30, 55 },
            //        new int[] { 1, 30, 5 })));
            //Console.WriteLine(string.Join(",",
            //    priorityPrinter(
            //        new int[] { 2, 1, 3, 2 },
            //        2)));
            //Console.WriteLine(string.Join(",",
            //    priorityPrinter(
            //        new int[] { 1, 1, 9, 1, 1, 1 },
            //        0)));
            //Console.WriteLine(string.Join(",",
            //    priorityPrinter(
            //        new int[] { 1, 1, 1, 2, 3, 1 },
            //        0)));
            //Console.WriteLine(findSosu("2713"));
            //Console.WriteLine(convertWord("hit", "cog", new string[] { "hot", "dot", "dog", "lot", "log", "cog" }));
            //Console.WriteLine(kakaoTaxiRoot(6, 4, 6, 2, new int[,] { { 4, 1, 10 }, { 3, 5, 24 }, { 5, 6, 2 }, { 3, 1, 41 }, { 5, 1, 24 }, { 4, 6, 50 }, { 2, 4, 66 }, { 2, 3, 22 }, { 1, 6, 25 } }));
            //Console.WriteLine(kakaoTaxiRoot(7, 3, 4, 1, new int[,] { { 5, 7, 9 }, { 4, 6, 4 }, { 3, 6, 1 }, { 3, 2, 3 }, { 2, 1, 6 } }));
            //Console.WriteLine(kakaoTaxiRoot(6, 4, 5, 6, new int[,] { { 2, 6, 6 }, { 6, 3, 7 }, { 4, 6, 7 }, { 6, 5, 11 }, { 2, 5, 12 }, { 5, 3, 20 }, { 2, 4, 8 }, { 4, 3, 9 } }));
            //minimumBribes(new int[] { 1, 2, 5, 3, 4, 7, 8, 6 });
            //Console.WriteLine(string.Join("\n",
            //    crosswordPuzzle(
            //        new string[]
            //        {
            //            "++++++-+++",
            //            "++------++",
            //            "++++++-+++",
            //            "++++++-+++",
            //            "+++------+",
            //            "++++++-+-+",
            //            "++++++-+-+",
            //            "++++++++-+",
            //            "++++++++-+",
            //            "++++++++-+"
            //        },
            //        "ICELAND;MEXICO;PANAMA;ALMATY")));
            //Console.WriteLine(gymSuit(5, new int[] { 2, 4 }, new int[] { 1, 3, 5 }));
            //Console.WriteLine(joystickCount("JAN"));
            //Console.WriteLine(priorityDiskController(new int[,] { { 0, 3 }, { 1, 9 }, { 2, 6 } }));
            //Console.WriteLine(string.Join(", ",
            //    kakaoSetMenu(
            //        new string[]
            //        {
            //            "ABCFG", "AC", "CDE", "ACDE", "BCFG", "ACDEH"
            //        },
            //        new int[] { 2, 3, 4 })));
            //Console.WriteLine(string.Join(", ",
            //    kakaoSetMenu(
            //        new string[]
            //        {
            //            "ABCDE", "AB", "CD", "ADE", "XYZ", "XYZ", "ACD"
            //        },
            //        new int[] { 2, 3, 5 })));
            //Console.WriteLine(string.Join(", ",
            //    kakaoSetMenu(
            //        new string[]
            //        {
            //            "XYZ", "XWY", "WXA"
            //        },
            //        new int[] { 2, 3, 4 })));
            //Console.WriteLine(string.Join(", ",
            //    kakaoApplicant(
            //        new string[]
            //        {
            //            "java backend junior pizza 150","python frontend senior chicken 210","python frontend senior chicken 150","cpp backend senior pizza 260","java backend junior chicken 80","python backend senior chicken 50"
            //        },
            //        new string[] { "java and backend and junior and pizza 100", "python and frontend and senior and chicken 200", "cpp and - and senior and pizza 250", "- and backend and senior and - 150", "- and - and - and chicken 100", "- and - and - and - 150" })));


            //Console.WriteLine(string.Join(", ",
            //    validateAddresses(
            //        new string[]
            //        {
            //            "1.003.160.035", "183.198.179.231", "78.172.104.155"
            //        }.ToList())));


            Console.WriteLine(minOperations(8));




            //kakaoMap1();
            //kakaoMap2();

            Console.Read();
        }

        public static long minOperations(long n)
        {
            // 1. (i+1)번째 바이너리가 1이고 모든 다른 바이너리들이 (i+2)번째부터 끝까지 제로일 경우 i를 바꾼다.
            // 2. 가장 오른쪽의 디짓은 조건 없이 바꿀 수 있다.
            char[] binaries = Convert.ToString(n, 2).ToArray();

            long count = 0;
            // int32가 아니라 64인데 ....
            while (Convert.ToInt32(new string(binaries), 2) != 0)
            {
                long prev_count = count;

                binaries[binaries.Count() - 1] = binaries[binaries.Count() - 1] == '0' ? '1' : '0';
                count++;

                for (int i = 0; i < binaries.Count() - 1; i++)
                {
                    if (binaries[i + 1] == '1')
                    {
                        if (i + 2 < binaries.Count())
                        {
                            int j = i + 2;
                            for (; j < binaries.Count(); j++)
                            {
                                if (binaries[j] == '1')
                                {
                                    break;
                                }
                            }
                            if (j == binaries.Count())
                            {
                                binaries[i] = binaries[i] == '0' ? '1' : '0';
                                count++;
                                break;
                            }
                        }
                        else
                        {
                            binaries[i] = binaries[i] == '0' ? '1' : '0';
                            count++;
                            break;
                        }
                    }
                }
            }

            return count;
        }

        public struct EventLog
        {
            public string Message;
            public string Time;
        }

        public static List<string> getEventsOrder(string team1, string team2, List<string> events1, List<string> events2)
        {
            List<EventLog> eventLogs = new List<EventLog>();
            foreach(var team1_event in events1)
            {
                var splitedEvent = team1_event.Split(' ').ToList();

                string last = splitedEvent.Last();

                if (last == "Y" || last == "G" || last == "R")
                {
                    string time = splitedEvent[splitedEvent.Count() - 2];
                    Console.WriteLine(time);

                    eventLogs.Add(new EventLog
                    {
                        Message = team1 + " " + team1_event,
                        Time = time
                    });
                }
                else
                {
                    string time = "";
                    foreach(var ele in splitedEvent)
                    {
                        if (ele == "S")
                        {
                            break;
                        }
                        time = ele;
                    }
                    Console.WriteLine(time);
                    eventLogs.Add(new EventLog
                    {
                        Message = team1 + " " + team1_event,
                        Time = time
                    });
                }
            }

            foreach (var team2_event in events2)
            {
                var splitedEvent = team2_event.Split(' ').ToList();

                string last = splitedEvent.Last();

                if (last == "Y" || last == "G" || last == "R")
                {
                    string time = splitedEvent[splitedEvent.Count() - 2];
                    Console.WriteLine(time);

                    eventLogs.Add(new EventLog
                    {
                        Message = team2 + " " + team2_event,
                        Time = time
                    });
                }
                else
                {
                    string time = "";
                    foreach (var ele in splitedEvent)
                    {
                        if (ele == "S")
                        {
                            break;
                        }
                        time = ele;
                    }
                    Console.WriteLine(time);

                    eventLogs.Add(new EventLog
                    {
                        Message = team2 + " " + team2_event,
                        Time = time
                    });
                }
            }

            eventLogs.Sort((l, r) =>
            {
                var lTimes = l.Time.Split('+');
                var rTimes = r.Time.Split('+');

                int l_first = int.Parse(lTimes[0]);
                int r_first = int.Parse(rTimes[0]);

                if(l_first - r_first == 0)
                {
                    int l_second = lTimes.Length > 1 ? int.Parse(lTimes[1]) : 0;
                    int r_second = rTimes.Length > 1 ? int.Parse(rTimes[1]) : 0;

                    return l_second - r_second;
                }
                else
                {
                    return l_first - r_first;
                }
            });

            return eventLogs.ConvertAll(v => v.Message);
        }

        public static List<string> validateAddresses(List<string> addresses)
        {
            string ipv4_pattern = @"\A([0]{0,2}[0-9]|[0]?[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0]{0,2}[0-9]|[0]?[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0]{0,2}[0-9]|[0]?[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\.([0]{0,2}[0-9]|[0]?[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\Z";
            string ipv6_pattern = @"(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))";

            Regex ip4_regex = new Regex(ipv4_pattern);
            Regex ip6_regex = new Regex(ipv6_pattern);

            List<string> result = new List<string>();

            foreach(var addr in addresses)
            {
                if(ip4_regex.IsMatch(addr))
                {
                    result.Add("IPv4");
                }
                else if (ip6_regex.IsMatch(addr))
                {
                    result.Add("IPv6");
                }
                else
                {
                    result.Add("Neither");
                }
            }

            return result;
        }

        // 카카오 조건에 맞는 지원자 찾기. 조합. 완전 탐색. 효율성
        public static int[] kakaoApplicant(string[] info, string[] query)
        {
            List<int> result = new List<int>();

            Dictionary<string, List<int>> ApplicantGroups = new Dictionary<string, List<int>>();

            foreach(var inf in info)
            {
                List<string[]> allGroupResult = new List<string[]>();

                List<string> infoElem = new List<string>(inf.Split(' '));
                int point = int.Parse(infoElem.Last());
                infoElem.RemoveAt(infoElem.Count - 1);

                applicantCombination(infoElem.ToArray(), allGroupResult);

                foreach(var group in allGroupResult)
                {
                    string key = string.Join(" and ", group);

                    if(!ApplicantGroups.ContainsKey(key))
                    {
                        ApplicantGroups[string.Join(" and ", group)] = new List<int>();
                    }
                    ApplicantGroups[string.Join(" and ", group)].Add(point);
                }
            }

            foreach(var q in query)
            {
                int lastSpaceIdx = q.LastIndexOf(' ');
                string key = q.Substring(0, lastSpaceIdx);
                int point = int.Parse(q.Substring(lastSpaceIdx + 1));

                result.Add(ApplicantGroups[key].FindAll(v => v >= point).Count());
            }

            return result.ToArray();
        }

        public static void applicantCombination(string[] arr, List<string[]> resultList, int baseIdx = 0, List<string> selected = null)
        {
            if (selected == null)
            {
                selected = new List<string>();
            }

            if (baseIdx < arr.Length)
            {
                selected.Add(arr[baseIdx]);
                applicantCombination(arr, resultList, baseIdx + 1, selected);
                selected.RemoveAt(selected.Count - 1);

                selected.Add("-");
                applicantCombination(arr, resultList, baseIdx + 1, selected);
                selected.RemoveAt(selected.Count - 1);
            }
            else
            {
                resultList.Add(selected.ToArray());
            }
        }

        // 카카오 세트 메뉴 찾기. 조합. 완전 탐색
        public static string[] kakaoSetMenu(string[] orders, int[] course)
        {
            HashSet<string> dan_pums = new HashSet<string>();
            foreach (var order in orders)
            {
                foreach (var dan_pum in order)
                {
                    dan_pums.Add(dan_pum.ToString());
                }
            }

            List<string[]> allComboResult = new List<string[]>();

            fullSizeCombination(dan_pums.OrderBy(v => v).ToArray(), allComboResult);

            Dictionary<string, int> comboOrderCount = new Dictionary<string, int>();
            foreach (string[] combo in allComboResult)
            {
                comboOrderCount[string.Join("", combo)] = 0;
            }

            foreach (string order in orders)
            {
                foreach (var combo in comboOrderCount.ToArray())
                {
                    for (int i = 0; i < combo.Key.Length; i++)
                    {
                        if (order.IndexOf(combo.Key[i]) == -1)
                        {
                            break;
                        }
                        if (i + 1 == combo.Key.Length)
                        {
                            comboOrderCount[combo.Key]++;
                        }
                    }
                }
            }

            List<string> result = new List<string>();
            foreach (var c in course)
            {
                var cCombo = comboOrderCount.ToList().FindAll(v =>
                {
                    if (v.Key.Length < 2 || v.Value < 2)
                    {
                        return false;
                    }
                    if (v.Key.Length == c)
                    {
                        return true;
                    }
                    return false;
                });
                if(cCombo.Count > 0)
                {
                    int maxCount = cCombo.Max(v => v.Value);

                    result.AddRange(cCombo.FindAll(v => v.Value == maxCount).ConvertAll(v => v.Key));
                }
            }
            result.Sort((l, r) => l.CompareTo(r));
            return result.ToArray();
        }

        // 이중 우선순위 큐. 힙
        public int[] DualPriorityQueue(string[] operations)
        {
            List<int> priQueue = new List<int>();

            foreach(var oper in operations)
            {
                var splitOper = oper.Split(' ');
                switch(splitOper[0])
                {
                    case "I":
                    {
                        int num = int.Parse(splitOper[1]);
                        if(priQueue.Count > 0)
                        {
                            int min = priQueue.First();
                            int max = priQueue.Last();

                            if(num <= min)
                            {
                                priQueue.Insert(0, num);
                            } else if(num >= max)
                            {
                                priQueue.Add(num);
                            }
                            else
                            {
                                priQueue.Add(num);
                                priQueue.Sort((l, r) =>
                                {
                                    return l - r;
                                });
                            }
                        }
                        else
                        {
                            priQueue.Add(num);
                        }
                        break;
                    }
                    case "D":
                    {
                        if(priQueue.Count > 0)
                        {
                            int num = int.Parse(splitOper[1]);
                            if (num == 1)
                            {
                                priQueue.RemoveAt(priQueue.Count - 1);
                            }
                            else if(num == -1)
                            {
                                priQueue.RemoveAt(0);
                            }
                        }
                        break;
                    }
                }
            }

            if(priQueue.Count == 0)
            {
                return new int[] { 0, 0 };
            }
            else
            {
                return new int[] { priQueue.Last(), priQueue.First() };
            }
        }

        // 우선순위 큐 평균 처리 시간 구하기. 힙
        public static int priorityDiskController(int[,] jobs)
        {
            List<int[]> jobList = new List<int[]>();
            for(int i = 0; i < jobs.GetLength(0); i++)
            {
                jobList.Add(new int[] { jobs[i, 0], jobs[i, 1] });
            }

            int currentTime = 0;
            int sumOfWaitTime = 0;
            while(jobList.Count > 0)
            {
                jobList.Sort((l, r) =>
                {
                    if(l[0] <= currentTime && r[0] <= currentTime)
                    {
                        return l[1] - r[1]; // 오름차순
                    }
                    else
                    {
                        return l[0] - r[0]; // 오름차순
                    }
                });

                var job = jobList[0];

                if(job[0] <= currentTime)
                {
                    sumOfWaitTime += currentTime - job[0] + job[1];
                    currentTime += job[1];
                    jobList.RemoveAt(0);
                } else
                {
                    currentTime = job[0];
                }
            }

            return sumOfWaitTime / jobs.GetLength(0);
        }

        // 조이스틱으로 문자열 똑같이 맞추는데 걸리는 최소 시간 구하기. 그리디. 탐욕
        public static int joystickCount(string name)
        {
            int count = 0;
            int size = name.Length;
            char[] targetName = name.ToArray();
            char[] inputName = Enumerable.Repeat<char>('A', name.Length).ToArray();
            int position = 0;

            while (true)
            {
                if(targetName[position] != inputName[position])
                {
                    int diff = targetName[position] - inputName[position];
                    if(diff > 12)
                    {
                        diff = 26 - diff;
                    }
                    count += diff;
                    targetName[position] = inputName[position];
                }

                int rightDist = 1;
                int rNewPosi = position;
                for (; rightDist < name.Length; rightDist++)
                {
                    rNewPosi = (rightDist + position) % name.Length;
                    if(targetName[rNewPosi] != inputName[rNewPosi])
                    {
                        break;
                    }
                }

                if(rightDist == name.Length)
                {
                    break;
                }

                int leftDist = 1;
                int lNewPosi = position;
                for (; leftDist < name.Length; leftDist++)
                {
                    lNewPosi = (position + name.Length - leftDist) % name.Length;
                    if (targetName[lNewPosi] != inputName[lNewPosi])
                    {
                        break;
                    }
                }
                if(rightDist > leftDist)
                {
                    position = lNewPosi;
                    count += leftDist;
                }
                else
                {
                    position = rNewPosi;
                    count += rightDist;
                }
            }

            return count;
        }

        // 체육복 빌려주기. 그리디. 탐욕법
        public static int gymSuit(int n, int[] lost, int[] reserve)
        {
            List<int> lostList = new List<int>(lost);
            List<int> reserveList = new List<int>(reserve);


            for (int i = 0; i < reserveList.Count(); i++)
            {
                for (int j = 0; j < lostList.Count(); j++)
                {
                    if (reserveList[i] == lostList[j])
                    {
                        lostList.RemoveAt(j);
                        reserveList.RemoveAt(i);
                        i--;
                        break;
                    }
                }
            }

            for (int i = 0; i < reserveList.Count; i++)
            {
                for (int j = 0; j < lostList.Count(); j++)
                {
                    if (reserveList[i] - 1 == lostList[j] || reserveList[i] + 1 == lostList[j])
                    {
                        lostList.RemoveAt(j);
                        break;
                    }
                }
            }

            return n - lostList.Count();
        }

        // 약자, 약어 문자 변환 가능 여부 찾기
        static bool[] abbreviationDP;

        static string abbreviation(string a, string b)
        {
            abbreviationDP = new bool[a.Length];
            return abbreviationRecur(0, 0, a, b) ? "YES" : "NO";
        }

        static bool abbreviationRecur(int start, int passCount, string a, string b)
        {
            if(passCount == b.Length)
            {
                for(int i = start; i < a.Length; i++)
                {
                    if(a[i] < 'a')
                    {
                        return false;
                    }
                }

                return true;
            }

            if(start >= a.Length)
            {
                return false;
            }

            if (abbreviationDP[start] && passCount == 0)
            {
                return false;
            }

            if (a.Length - start < b.Length - passCount)
            {
                return false;
            }

            for (int i = start; i < a.Length - passCount; i++)
            {
                if (a[i] == b[passCount])
                {
                    if (abbreviationRecur(i + 1, passCount + 1, a, b))
                    {
                        return true;
                    }
                }
                else if (a[i] - 32 == b[passCount])
                {
                    if (abbreviationRecur(i + 1, passCount + 1, a, b))
                    {
                        return true;
                    }
                    else if (abbreviationRecur(i + 1, passCount, a, b))
                    {
                        return true;
                    }
                }
                else if (a[i] >= 'a')
                {
                    if (abbreviationRecur(i + 1, passCount, a, b))
                    {
                        return true;
                    }
                }
                else
                {
                    break;
                }
            }

            if(passCount == 0)
            {
                abbreviationDP[start] = true;
            }

            return false;
        }

        // Sub 가장 합이 큰 서브셋의 경우의 수 찾기 다른 사람이 짠거
        static int maxSubsetSum(int[] arr)
        {
            if (arr.Length < 3)
            {
                return 0;
            }

            int[] dp = new int[arr.Length];

            dp[0] = Math.Max(arr[0], 0);
            dp[1] = Math.Max(arr[1], 0);

            for(int i = 2; i < arr.Length; i++)
            {
                dp[i] = Math.Max(Math.Max(arr[i] + dp[i - 2], dp[i - 1]), dp[i - 2]);
            }

            return Math.Max(dp[dp.Length - 1], dp[dp.Length - 2]);
        }

        static Dictionary<int, int> maxSubsetCache = new Dictionary<int, int>();

        // 가장 합이 큰 서브셋 경우의 수 찾기
        static int maxSubsetSumMe(int[] arr)
        {
            if (arr.Length < 3)
            {
                return 0;
            }

            int plusCount = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] >= 0)
                {
                    plusCount++;
                    if (plusCount == 1)
                    {
                        plusCount++;
                        i += 2;
                    }
                    if (plusCount == 2)
                    {
                        break;
                    }
                }
            }
            if (plusCount == 0)
            {
                return 0;
            }

            return Math.Max(0, maxSubsetSumRecur(0, arr, 0, plusCount == 2, false));
        }

        static int maxSubsetSumRecur(int start, int[] arr, int depth, bool isTwicePlus, bool isSelectedMinus)
        {
            if (start >= arr.Length)
            {
                return 0;
            }

            if (maxSubsetCache.ContainsKey(start))
            {
                return maxSubsetCache[start];
            }

            int subMax = int.MinValue;

            for (int i = start; i < arr.Length; i++)
            {
                if (depth > 0)
                {
                    subMax = Math.Max(subMax, arr[i]);
                }

                if (arr[i] < 0 && (isSelectedMinus || isTwicePlus))
                {
                    continue;
                }

                if (i + 2 < arr.Length)
                {
                    int r = maxSubsetSumRecur(i + 2, arr, depth + 1, isTwicePlus, arr[i] < 0 || isSelectedMinus);

                    subMax = Math.Max(subMax, r + arr[i]);
                }
            }

            maxSubsetCache[start] = subMax;

            return subMax;
        }

        // super digit 슈퍼 디짓 찾기. 재귀
        static int superDigit(string n, int k)
        {
            return superDigitRecur(n, k);
        }

        static int superDigitRecur(string n, int k)
        {
            if(n.Length <= 0 || k <= 0)
            {
                return 0;
            }
            if(n.Length * k == 1)
            {
                return int.Parse(n);
            }
            long sum = 0;
            foreach(var d in n)
            {
                sum += d - '0';
            }
            return superDigitRecur((sum * k).ToString(), 1);
        }

        // Crossword 단어 끼워넣기 퍼즐. 재귀
        static string[] crosswordPuzzle(string[] crossword, string words)
        {
            HashSet<string> wordSet = new HashSet<string>(words.Split(';'));

            return crosswordPuzzleRecur(crossword, wordSet);
        }

        static string[] crosswordPuzzleRecur(string[] map, HashSet<string> words)
        {
            if(words.Count == 0)
            {
                return map;
            }
            else
            {
                Regex emptyPattern = new Regex(@"([^+]*-+[^+]*-*)");

                // 가로 탐색
                for (int i = 0; i < map.Length; i++)
                {
                    string str = map[i];
                    var match = emptyPattern.Match(str);
                    if (match.Success)
                    {
                        string wordPattern = "\\A" + match.ToString().Replace('-', '.') + "\\Z";
                        foreach (var word in words.ToArray())
                        {
                            if (Regex.IsMatch(word, wordPattern))
                            {
                                words.Remove(word);
                                var copiedMap = map.ToArray();
                                copiedMap[i] = emptyPattern.Replace(str, word, 1);
                                var r = crosswordPuzzleRecur(copiedMap, words);
                                if (r != null)
                                {
                                    return r;
                                }
                                words.Add(word);
                            }
                        }
                    }
                }

                // 세로 탐색
                for (int i = 0; i < map[0].Length; i++)
                {
                    string str = "";
                    for (int j = 0; j < map.Length; j++)
                    {
                        str += map[j][i];
                    }

                    var match = emptyPattern.Match(str);
                    if (match.Success)
                    {
                        string wordPattern = "\\A" + match.ToString().Replace('-', '.') + "\\Z";
                        foreach (var word in words.ToArray())
                        {
                            if (Regex.IsMatch(word, wordPattern))
                            {
                                words.Remove(word);
                                var copiedMap = map.ToArray();
                                string convertedStr = emptyPattern.Replace(str, word, 1);
                                for (int j = 0; j < map.Length; j++)
                                {
                                    copiedMap[j] = map[j].Substring(0, i) + convertedStr[j];
                                    if ((i + 1) < map[j].Length)
                                    {
                                        copiedMap[j] += map[j].Substring(i + 1, map[j].Length - (i + 1));
                                    }
                                }
                                var r = crosswordPuzzleRecur(copiedMap, words);
                                if (r != null)
                                {
                                    return r;
                                }
                                words.Add(word);
                            }
                        }
                    }
                }
            }
            return null;
        }

        // 계단 오르는 방법 경우의 수. 재귀, DFS?, DP
        static int stepPerms(int n)
        {
            return stepPermsRecur(0, 0, n);
        }

        static Dictionary<int, int> stairCache = new Dictionary<int, int>();

        static int stepPermsRecur(int step, int position, int n)
        {
            position += step;
            int left = n - position;

            if(stairCache.ContainsKey(left))
            {
                return stairCache[left];
            }

            int result = 0;
            if (n > position)
            {
                result = stepPermsRecur(1, position, n);
                if(n >= position + 3)
                {
                    result += stepPermsRecur(2, position, n) + stepPermsRecur(3, position, n);
                }
                else if (n >= position + 2)
                {
                    result += stepPermsRecur(2, position, n);
                }
            }
            else if (n < position)
            {
                result = 0;
            }
            else
            {
                result = 1;
            }
            stairCache[left] = result;
            return result;
        }

        // 피보나치, 재귀, DP
        public static int[] fiboCache = new int[100];

        public static int Fibonacci(int n)
        {
            if (n < 2)
            {
                return n;
            }
            if (fiboCache[n - 2] > 0)
            {
                return fiboCache[n - 2];
            }
            fiboCache[n - 2] = Fibonacci(n - 1) + Fibonacci(n - 2);
            return fiboCache[n - 2];
        }

        // 프리퀀시 큐?
        static List<int> freqQuery(List<List<int>> queries)
        {
            List<int> result = new List<int>();

            Dictionary<int, int> numCount = new Dictionary<int, int>();
            Dictionary<int, int> counts = new Dictionary<int, int>();
            foreach (var query in queries)
            {
                switch(query[0])
                {
                    case 1:
                    {
                        if (numCount.ContainsKey(query[1]))
                        {
                            if (numCount[query[1]] > 0)
                            {
                                counts[numCount[query[1]]]--;
                            }
                            numCount[query[1]]++;
                        }
                        else
                        {
                            numCount[query[1]] = 1;
                        }
                        if (counts.ContainsKey(numCount[query[1]]))
                        {
                            counts[numCount[query[1]]]++;
                        }
                        else
                        {
                            counts[numCount[query[1]]] = 1;
                        }
                        break;
                    }
                    case 2:
                    {
                        if (numCount.ContainsKey(query[1]) && numCount[query[1]] > 0)
                        {
                            counts[numCount[query[1]]]--;
                            numCount[query[1]]--;
                            if(numCount[query[1]] > 0)
                            {
                                counts[numCount[query[1]]]++;
                            }
                        }
                        break;
                    }
                    case 3:
                    {
                        if (counts.ContainsKey(query[1]) && counts[query[1]] > 0)
                        {
                            result.Add(1);
                        }
                        else
                        {
                            result.Add(0);
                        }
                        break;
                    }
                }
            }

            return result;
        }

        // 배수의 연속 경우의 수 찾기. r이 1일때는 해결 못 했음
        static long countTriplets(List<long> arr, long r)
        {
            long count = 0;

            long first = 1;
            long second = first * r;
            long third = second * r;

            long firstCount = 0;
            long secondCount = 0;
            long thirdCount = 0;

            foreach (long num in arr)
            {
                if(num > third)
                {
                    count += firstCount * secondCount * thirdCount;

                    long prev = num / r;
                    long prev2 = prev / r;

                    if (prev == third)
                    {
                        first = second;
                        second = third;
                        third = num;

                        firstCount = secondCount;
                        secondCount = thirdCount;
                        thirdCount = 1;
                    }
                    else if (prev2 == third)
                    {
                        first = third;
                        second = prev;
                        third = num;

                        firstCount = thirdCount;
                        secondCount = 0;
                        thirdCount = 1;
                    }
                    else
                    {
                        first = prev2;
                        second = prev;
                        third = num;

                        firstCount = 0;
                        secondCount = 0;
                        thirdCount = 1;
                    }
                }
                else if(num == third)
                {
                    thirdCount++;
                }
                else if(num == second)
                {
                    secondCount++;
                }
                else if(num == first)
                {
                    firstCount++;
                }
            }
            count += firstCount * secondCount * thirdCount;

            return count;
        }

        // 아나그램 경우의 수 찾기
        static int sherlockAndAnagrams(string s)
        {
            int count = 0;

            Dictionary<string, int> anagramCount = new Dictionary<string, int>();
            for (int i = 0; i < s.Length; i++)
            {
                for(int j = 1; j <= s.Length - i; j++)
                {
                    string hubo = string.Concat(s.Substring(i, j).OrderBy(v => v));
                    if(anagramCount.ContainsKey(hubo))
                    {
                        count += anagramCount[hubo];
                        anagramCount[hubo]++;
                    }
                    else
                    {
                        anagramCount[hubo] = 1;
                    }
                }
            }

            return count;
        }

        // 공유하는 문자가 있는지 체크
        static string twoStrings(string s1, string s2)
        {
            string shortStr = s1.Length < s2.Length ? s1 : s2;
            string longStr = s1.Length < s2.Length ? s2 : s1;

            HashSet<char> longStrChars = new HashSet<char>(longStr);

            foreach(var c in shortStr)
            {
                if(longStrChars.Contains(c))
                {
                    return "YES";
                }
            }

            return "NO";
        }

        // 매거진, 노트의 단어 체크
        static void checkMagazine(string[] magazine, string[] note)
        {
            Dictionary<string, int> magazDict = new Dictionary<string, int>();

            foreach (var m_word in magazine)
            {
                if (magazDict.ContainsKey(m_word))
                {
                    magazDict[m_word]++;
                }
                else
                {
                    magazDict[m_word] = 1;
                }
            }

            foreach (var n_word in note)
            {
                if(!magazDict.ContainsKey(n_word) || magazDict[n_word] == 0)
                {
                    Console.WriteLine("No");
                    return;
                }

                magazDict[n_word]--;
            }
            Console.WriteLine("Yes");
        }

        // 배열의 특정 범위에 값 넣고 최댓값 찾기. 천잰가 이거 생각한 사람은..?
        static long arrayManipulation(int n, int[][] queries)
        {
            long[] arr = new long[n];

            foreach (var query in queries)
            {
                int a = query[0];
                int b = query[1];
                int k = query[2];

                if (k > 0)
                {
                    arr[a - 1] += k;
                    if((b) < n)
                    {
                        arr[b] -= k;
                    }
                }
            }

            long max = 0;
            long sum = 0;
            foreach(var v in arr)
            {
                if(v == 0)
                {
                    continue;
                }
                sum += v;
                max = Math.Max(max, sum);
            }

            return max;
        }

        // 최소 스왑 횟수 구하기
        static int minimumSwaps(int[] arr)
        {
            int count = 0;
            for(int i = 0; i < arr.Length;)
            {
                if(arr[i] != i + 1)
                {
                    int tmp = arr[i];
                    arr[i] = arr[tmp - 1];
                    arr[tmp - 1] = tmp;
                    count++;
                }
                else
                {
                    i++;
                }
            }

            return count;
        }

        // 뇌물 새치기
        static void minimumBribes(int[] q)
        {
            int count = 0;
            for (int i = q.Length - 1; i >= 0; i--)
            {
                if (q[i] != (i + 1))
                {
                    if ((i - 1) >= 0 && q[i - 1] == (i + 1))
                    {
                        q[i - 1] = q[i];
                        q[i] = i + 1;
                        count += 1;
                    }
                    else if ((i - 2) >= 0 && q[i - 2] == (i + 1))
                    {
                        q[i - 2] = q[i - 1];
                        q[i - 1] = q[i];
                        q[i] = i + 1;
                        count += 2;
                    }
                    else
                    {
                        Console.WriteLine("Too chaotic");
                        return;
                    }
                }
            }

            Console.WriteLine(count);
        }

        // 배열 로테이트
        static int[] rotLeft(int[] a, int d)
        {
            int[] rotated = new int[a.Length];

            for(int i = 0; i < a.Length; i++)
            {
                rotated[i] = a[(i + d) % a.Length];
            }

            return rotated;
        }

        // 모래시계
        static int hourglassSum(int[][] arr)
        {
            int result = int.MinValue;

            for(int y = 0; y < 4; y++)
            {
                for(int x = 0; x < 4; x++)
                {
                    int sum = 0;
                    sum += arr[y][x] + arr[y][x + 1] + arr[y][x + 2];
                    sum += arr[y + 1][x + 1];
                    sum += arr[y + 2][x] + arr[y + 2][x + 1] + arr[y + 2][x + 2];
                    result = Math.Max(result, sum);
                }
            }
            return result;
        }

        // 카카오 택시 최단 경로 찾기
        public static int kakaoTaxiRoot(int n, int s, int a, int b, int[,] fares) {
            Dictionary<int, Dictionary<int, int>> hashedFares = new Dictionary<int, Dictionary<int, int>>();
            for(int i = 0; i < fares.GetLength(0); i++)
            {
                int stt = fares[i, 0];
                int dst = fares[i, 1];
                int cst = fares[i, 2];

                if(!hashedFares.ContainsKey(stt))
                {
                    hashedFares[stt] = new Dictionary<int, int>();
                }
                if (!hashedFares.ContainsKey(dst))
                {
                    hashedFares[dst] = new Dictionary<int, int>();
                }
                hashedFares[stt][dst] = cst;
                hashedFares[dst][stt] = cst;
            }
            bool[] a_flags = new bool[n + 1];
            bool[] b_flags = new bool[n + 1];

            return kakaoTaxiRoot_DFS(0, 3, n, s, a, b, hashedFares, a_flags, b_flags);
        }

        // who: 1 = a, 2 = b, 3 = ab
        public static int kakaoTaxiRoot_DFS(int costSum, int who, int n, int start, int desti_a, int desti_b, Dictionary<int, Dictionary<int, int>> fares, bool[] a_flags, bool[] b_flags)
        {
            if (who == 3)
            {
                a_flags[start] = true;
                b_flags[start] = true;

                if (a_flags[desti_a] && b_flags[desti_b])
                {
                    a_flags[start] = false;
                    b_flags[start] = false;
                    return costSum;
                }
                else if(a_flags[desti_a])
                {
                    a_flags[start] = false;
                    who = 2;
                }
                else if (b_flags[desti_b])
                {
                    b_flags[start] = false;
                    who = 1;
                }
            }
            else if(who == 1)
            {
                a_flags[start] = true;

                if (a_flags[desti_a])
                {
                    a_flags[start] = false;
                    return costSum;
                }
            }
            else if (who == 2)
            {
                b_flags[start] = true;

                if (b_flags[desti_b])
                {
                    b_flags[start] = false;
                    return costSum;
                }
            }

            int cost = int.MaxValue;
            if(fares.ContainsKey(start))
            {
                foreach(var dstCst in fares[start])
                {
                    if (who == 3)
                    {
                        if (!a_flags[dstCst.Key] && !b_flags[dstCst.Key])
                        {
                            cost = Math.Min(cost, kakaoTaxiRoot_DFS(costSum + dstCst.Value, who, n, dstCst.Key, desti_a, desti_b, fares, a_flags, b_flags));
                        }
                        if (!a_flags[dstCst.Key])
                        {
                            int costA = int.MaxValue;
                            int costB = int.MaxValue;
                            // A만 보내고 B는 다른곳으로 보내보기
                            if ((costA = kakaoTaxiRoot_DFS(dstCst.Value, 1, n, dstCst.Key, desti_a, desti_b, fares, a_flags, b_flags)) != int.MaxValue)
                            {
                                foreach (var dstCst_B in fares[start])
                                {
                                    if (!dstCst.Equals(dstCst_B) && !b_flags[dstCst_B.Key] && (costB = kakaoTaxiRoot_DFS(dstCst_B.Value, 2, n, dstCst_B.Key, desti_a, desti_b, fares, a_flags, b_flags)) != int.MaxValue)
                                    {
                                        cost = Math.Min(cost, costA + costB + costSum);
                                    }
                                }
                            }
                        }
                    }
                    else if(who == 1 && !a_flags[dstCst.Key])
                    {
                        cost = Math.Min(cost, kakaoTaxiRoot_DFS(costSum + dstCst.Value, who, n, dstCst.Key, desti_a, desti_b, fares, a_flags, b_flags));
                    }
                    else if (who == 2 && !b_flags[dstCst.Key])
                    {
                        cost = Math.Min(cost, kakaoTaxiRoot_DFS(costSum + dstCst.Value, who, n, dstCst.Key, desti_a, desti_b, fares, a_flags, b_flags));
                    }
                }
            }

            if (who == 3)
            {
                a_flags[start] = false;
                b_flags[start] = false;
            }
            else if (who == 1)
            {
                a_flags[start] = false;
            }
            else if (who == 2)
            {
                b_flags[start] = false;
            }

            return cost;
        }

        // 카카오 신규 아이디 추천
        public static string kakaroNewId(string new_id)
        {
            // 1
            new_id = new_id.ToLower();
            // 2
            new_id = Regex.Replace(new_id, @"[^a-z0-9_\.-]", "");
            // 3
            new_id = Regex.Replace(new_id, @"\.+", ".");
            // 4
            new_id = Regex.Replace(new_id, @"(\A\.|\.\Z)", "");
            // 5
            new_id = new_id.Length == 0 ? "a" : new_id;
            // 6
            new_id = new_id.Length < 16 ? new_id : new_id.Substring(0, 15);
            new_id = Regex.Replace(new_id, @"(\A\.|\.\Z)", "");
            // 7
            for(int i = new_id.Length; i < 3; i++)
            {
                new_id += new_id[new_id.Length - 1];
            }

            // result
            return new_id;
        }

        // a의 빈도수
        static long repeatedString(string s, long n)
        {
            long sCount = 0;
            long leftCount = 0;
            long count = 0;
            for (int i = 0; i < s.Length; i++)
            {
                if (s[i] == 'a')
                {
                    sCount++;
                    if (i < n % s.Length)
                    {
                        leftCount++;
                    }
                }
            }
            count = (n / s.Length) * sCount + leftCount;

            return count;
        }

        // Jumping Clouds
        static int jumpingOnClouds(int[] c)
        {
            int count = 0;

            for (int i = 0; i < c.Length;)
            {
                if (i + 1 == c.Length)
                {
                    break;
                }
                else if (i + 2 >= c.Length)
                {
                    i += 1;
                }
                else if (c[i + 2] == 1)
                {
                    i += 1;
                }
                else
                {
                    i += 2;
                }
                count++;
            }

            return count;
        }

        // Valleys 계곡 찾기
        public static int countingValleys(int steps, string path)
        {
            int currentH = 0;
            bool isValley = false;
            int count = 0;
            for(int i = 0; i < steps; i++)
            {
                if(path[i] == 'U')
                {
                    currentH++;
                    if(isValley && currentH >= 0)
                    {
                        isValley = false;
                        count++;
                    }
                }
                else
                {
                    currentH--;
                    if (!isValley && currentH < 0)
                    {
                        isValley = true;
                    }
                }
            }

            return count;
        }

        // 공항 티켓 경로 찾기 DFS
        public string[] ticketRoot(string[,] tickets)
        {
            List<KeyValuePair<string, string>?> ticketList = new List<KeyValuePair<string, string>?>();
            for(int i = 0; i < tickets.GetLength(0); i++)
            {
                ticketList.Add(new KeyValuePair<string, string>(tickets[i, 0], tickets[i, 1]));
            }
            ticketList.Sort((l, r) =>
            {
                return l?.Value.CompareTo(r?.Value) ?? 0;
            });

            List<string> root = new List<string>();
            ticketRoot_DFS("ICN", root, ticketList);

            return root.ToArray();
        }

        public bool ticketRoot_DFS(string start, List<string> root, List<KeyValuePair<string, string>?> tickets)
        {
            root.Add(start);

            if (tickets.Count == 0)
            {
                return true;
            }
            else
            {
                foreach (var ticket in tickets.ToArray())
                {
                    if (ticket?.Key == start)
                    {
                        tickets.Remove(ticket);
                        if (ticketRoot_DFS(ticket?.Value, root, tickets))
                        {
                            return true;
                        }
                        else
                        {
                            tickets.Add(ticket);
                        }
                    }
                }
                root.RemoveAt(root.Count - 1);
                return false;
            }
        }

        // word 언어 변환 최단 거리 DFS
        public static int convertWord(string begin, string target, string[] words)
        {
            HashSet<string> root = new HashSet<string>();
            return words_DFS(root, begin, target, words);
        }

        public static int words_DFS(HashSet<string> root, string v, string target, string[] words)
        {
            if (target == v)
            {
                return root.Count;
            }

            root.Add(v);

            int result = 0;
            for (int i = 0; i < words.Length; i++)
            {
                if(root.Contains(words[i]))
                {
                    continue;
                }

                int diffCount = 0;
                for(int j = 0; j < v.Length; j++)
                {
                    if (v[j] != words[i][j])
                    {
                        diffCount++;
                    }
                }

                if(diffCount == 1)
                {
                    int r = words_DFS(root, words[i], target, words);
                    if(result == 0)
                    {
                        result = r;
                    }
                    else if(r > 0)
                    {
                        result = Math.Min(r, result);
                    }
                }
            }
            root.Remove(v);
            return result;
        }

        // 네트워크 숫자 DFS
        public static int countNetwork(int n, int[,] computers)
        {
            bool[] flag = new bool[n];

            int result = 0;
            for (int i = 0; i < n; i++)
            {
                if (countNetwork_dfs(i, flag, computers))
                {
                    result++;
                }
            }

            return result;
        }

        public static bool countNetwork_dfs(int computer, bool[] flag, int[,] computers)
        {
            if (flag[computer])
            {
                return false;
            }

            flag[computer] = true;

            for (int i = 0; i < flag.Length; i++)
            {
                if (computer != i && computers[computer, i] == 1)
                {
                    countNetwork_dfs(i, flag, computers);
                }
            }
            return true;
        }


        // 숫자의 빼기 더하기 경우의 수. DFS
        public static int targetNumber(int[] numbers, int target)
        {
            return target_number_dfs(0, 0, numbers, target);
        }

        public static int target_number_dfs(int v, int depth, int[] numbers, int target)
        {
            if (depth == numbers.Length)
            {
                return v == target ? 1 : 0;
            }

            return target_number_dfs(v - numbers[depth], depth + 1, numbers, target)
                + target_number_dfs(v + numbers[depth], depth + 1, numbers, target);
        }

        // 카펫 사이즈. 완전 탐색
        public int[] carpetSize(int brown, int yellow)
        {
            for (int w = 3; w <= (brown - 2) /2; w ++)
            {
                int h = (brown / 2) - (w - 1) + 1;

                if(w >= h && w * h == brown + yellow)
                {
                    return new int[] { w, h };
                }
            }
            return null;
        }

        // 소수 순열. 완전 탐색
        public static int findSosu(string numbers)
        {
            int answer = 0;
            var eachCharacters = numbers.ToList().ConvertAll(v => v.ToString());

            List<string[]> fullSizeResults = new List<string[]>();
            fullSizePermutation(eachCharacters.ToArray(), fullSizeResults);

            // 중복 데이터 제거
            HashSet<string> fullSizeNumbers = new HashSet<string>(fullSizeResults.Select(v => string.Join("", v)));
            HashSet<long> allSizeNumbers = new HashSet<long>();
            for (int i = 0; i < numbers.Length; i++)
            {
                var nums = fullSizeNumbers.Select(v => long.Parse(v.Substring(i)));
                foreach (long num in nums)
                {
                    allSizeNumbers.Add(num);
                }
            }
            foreach (long num in allSizeNumbers)
            {
                if (isPrime(num)) answer++;
            }
            return answer;
        }

        public static bool isPrime(long candidate) // 소수 판정
        {
            if (candidate % 2 == 0)
            {
                return candidate == 2;
            }
            for (int i = 3; (i * i) <= candidate; i += 2)
            {
                if ((candidate % i) == 0)
                {
                    return false;
                }
            }
            return candidate != 1;
        }

        // 순서에 따른 중복 o
        public static void fullSizePermutation(string[] arr, List<string[]> resultList, int baseIdx = 0)
        {
            if (baseIdx < arr.Length - 1)
            {
                for (int i = baseIdx; i < arr.Length; i++)
                {
                    // 스왑
                    string temp = arr[baseIdx];
                    arr[baseIdx] = arr[i];
                    arr[i] = temp;

                    fullSizePermutation(arr, resultList, baseIdx + 1);

                    // 스왑 복구
                    temp = arr[baseIdx];
                    arr[baseIdx] = arr[i];
                    arr[i] = temp;
                }
            }
            else
            {
                resultList.Add(arr.ToArray());
            }
        }

        // 순서에 따른 중복 x
        public static void fullSizeCombination(string[] arr, List<string[]> resultList, int baseIdx = 0, List<string> selected = null)
        {
            if(selected == null)
            {
                selected = new List<string>();
            }

            if (baseIdx < arr.Length)
            {
                for (int i = baseIdx; i < arr.Length; i++)
                {
                    selected.Add(arr[i]);
                    resultList.Add(selected.ToArray());
                    fullSizeCombination(arr, resultList, i + 1, selected);
                    selected.RemoveAt(selected.Count - 1);
                }
            }
        }

        public int[] solution(int[] answers)
        {
            int[] score = { 0, 0, 0 };

            List<Queue<int>> patterns = new List<Queue<int>>();
            patterns.Add(new Queue<int>(new int[] { 1, 2, 3, 4, 5 }));
            patterns.Add(new Queue<int>(new int[] { 2, 1, 2, 3, 2, 4, 2, 5 }));
            patterns.Add(new Queue<int>(new int[] { 3, 3, 1, 1, 2, 2, 4, 4, 5, 5 }));

            foreach (var answer in answers)
            {
                int[] a = { 0, 0, 0 };
                for (int i = 0; i < 3; i++)
                {
                    a[i] = patterns[i].Dequeue();
                    patterns[i].Enqueue(a[i]);
                    if (a[i] == answer)
                    {
                        score[i]++;
                    }
                }
            }

            List<int> result = new List<int>();
            int max = score.Max();

            for (int i = 0; i < 3; i++)
            {
                if (score[i] == max)
                {
                    result.Add(i + 1);
                }
            }

            return result.ToArray();
        }

        public struct PrintTarget
        {
            public int priority;
            public bool isTarget;
        }

        // 우선순위 프린터. 큐
        public static int priorityPrinter(int[] priorities, int location)
        {
            var queue = new Queue<PrintTarget>();

            for(int i = 0; i < priorities.Length; i++)
            {
                queue.Enqueue(new PrintTarget { priority = priorities[i], isTarget = i == location });
            }

            int result = 1;
            while(queue.Count > 0)
            {
                // 우선순위 가장 큰게 맨 앞으로 오도록
                var searchQueue = new Queue<PrintTarget>(queue);
                PrintTarget? current = null;
                foreach (var search in searchQueue.ToArray())
                {
                    if(current == null)
                    {
                        current = search;
                    }
                    else if (search.priority > current?.priority)
                    {
                        queue = new Queue<PrintTarget>(searchQueue);
                        current = search;
                    }
                    searchQueue.Enqueue(searchQueue.Dequeue());
                }

                if(queue.Dequeue().isTarget)
                {
                    return result;
                }
                else
                {
                    result++;
                }
            }

            return result;
        }

        // 우선순위 프린터. 큐. 다른사람이 푼거
        public int priorityPrinter2(int[] priorities, int location)
        {
            int answer = 0;

            Queue<KeyValuePair<int, int>> que = new Queue<KeyValuePair<int, int>>();
            for (int i = 0; i < priorities.Length; i++)
            {
                que.Enqueue(new KeyValuePair<int, int>(i, priorities[i]));
            }

            int nMax = que.Max(x => x.Value);
            while (true)
            {
                var kv = que.Dequeue();
                if (kv.Value == nMax)
                {
                    if (kv.Key == location) return answer + 1;
                    else
                    {
                        answer++;
                        nMax = que.Max(x => x.Value);
                        continue;
                    }
                }
                que.Enqueue(kv);
            }
        }

        // 개발 진행 완료 시기 구하기. 큐
        public static int[] workingProcess(int[] progresses, int[] speeds)
        {
            // 큐 사용 없이
            List<int> result = new List<int>();

            for(int base_idx = 0; base_idx < progresses.Length;)
            {
                int need_time = (100 - progresses[base_idx]) / speeds[base_idx]
                    + ((100 - progresses[base_idx]) % speeds[base_idx] == 0 ? 0 : 1);

                int j = base_idx + 1;
                for (; j < progresses.Length; j++)
                {
                    if (100 > progresses[j] + (need_time * speeds[j]))
                    {
                        break;
                    }
                }
                result.Add(j - base_idx);
                base_idx = j;
            }

            return result.ToArray();

            //// 큐 사용
            //List<int> result = new List<int>();
            //var queue = new Queue<int>(progresses);

            //int base_idx = 0;
            //while(queue.Count > 0)
            //{
            //    int base_process = queue.Dequeue();

            //    int need_time = (100 - base_process) / speeds[base_idx] + ((100 - base_process) % speeds[base_idx] == 0 ? 0 : 1);

            //    int count = 1;
            //    for(int j = base_idx + 1; j < progresses.Length; j++)
            //    {
            //        if(100 <= progresses[j] + (need_time * speeds[j]))
            //        {
            //            queue.Dequeue();
            //            count++;
            //        }
            //        else
            //        {
            //            base_idx = j;
            //            break;
            //        }
            //    }
            //    result.Add(count);
            //}

            //return result.ToArray();
        }

        // 개발 진행 완료 시기 구하기. 단순 무식하게
        public static int[] workingProcess2(int[] progresses, int[] speeds)
        {
            int arr_size = progresses.Count();
            bool[] complete_flags = new bool[arr_size];

            List<int> results = new List<int>();

            int count_complete = 0;
            while (count_complete < arr_size)
            {
                for (int i = count_complete; i < arr_size; i++)
                {
                    if (progresses[i] < 100)
                    {
                        progresses[i] += speeds[i];
                    }

                    if (progresses[i] >= 100)
                    {
                        complete_flags[i] = true;
                    }
                }

                for (int i = count_complete; i < arr_size + 1; i++)
                {
                    if (i >= arr_size || !complete_flags[i])
                    {
                        int count_current_complete = i - count_complete;
                        if (count_current_complete != 0)
                        {
                            results.Add(count_current_complete);
                            count_complete += count_current_complete;
                        }
                    }
                }
            }
            return results.ToArray();
        }

        // 주식 가격이 떨어지는 시점 구하기, 큐 없이
        public static int[] stockPrice(int[] prices)
        {
            int[] result = new int[prices.Length];

            for (int i = 0; i < prices.Length - 1; i++)
            {
                int count = 0;
                for(int j = i + 1; j < prices.Length; j++)
                {
                    count++;
                    if(prices[i] > prices[j])
                    {
                        break;
                    }
                }
                result[i] = count;
            }

            return result;
        }

        // 주식 가격이 떨어지는 시점 구하기, 큐 사용해서.. 느린데 왜지?
        public static int[] stockPriceUsingQueue(int[] prices)
        {
            var prices_queue = new Queue<int>(prices); // 요거랑
            int[] result = new int[prices.Length];

            for (int i = 0; i < prices.Length; i++)
            {
                var current_price = prices_queue.Dequeue(); // 요게 원인인가?

                foreach (var p in prices_queue)
                {
                    result[i]++;
                    if (current_price > p)
                    {
                        break;
                    }
                }
            }

            return result;
        }

        // 카카오 맵 암호 풀기
        static void kakaoMap1()
        {
            Console.WriteLine("Method1");
            int n = 5;
            int[] arr1 = { 9, 20, 28, 18, 11 };
            int[] arr2 = { 30, 1, 21, 17, 28 };

            for (int i = 0; i < n; i++)
            {
                string str = "";
                int num = arr1[i] | arr2[i];
                for (int j = 0; j < n; j++)
                {
                    str = (num % 2 == 0 ? " " : "#") + str;
                    num = num / 2;
                }
                Console.WriteLine(str);
            }
        }

        // 카카오 맵 암호 풀기
        static void kakaoMap2()
        {
            Console.WriteLine("Method2");
            int n = 5;
            int[] arr1 = { 9, 20, 28, 18, 11 };
            int[] arr2 = { 30, 1, 21, 17, 28 };

            for (int i = 0; i < n; i++)
            {
                string str = "";
                int num = arr1[i] | arr2[i];
                int targetBit = 1;
                for (int j = 0; j < n; j++)
                {
                    str = ((num & targetBit) == 0 ? " " : "#") + str;
                    targetBit = targetBit << 1;
                }
                Console.WriteLine(str);
            }
        }

        // harshad(하샤드) 수 구하기
        public static bool harshad(int x)
        {
            int[] dd = new int[3];
            bool[] aa = new bool[dd.Count()];

            int copy_x = x;

            int sum = 0;
            while(x != 0)
            {
                sum += x % 10;
                x = x / 10;
            }

            return copy_x % sum == 0;
        }

        // 피보나치
        public static int solution6(int n)
        {
            if (n <= 0)
            {
                return 0;
            }
            else if (n <= 2)
            {
                return 1;
            }
            else
            {
                int current = 0;
                int prev2 = 1;
                int prev1 = 1;
                for (int i = 2; i < n; i++)
                {
                    current = prev2 + prev1;
                    prev2 = prev1;
                    prev1 = current;
                }
                return current;
            }
        }

        // 양말 짝 개수 찾기
        static int sockMerchant(int n, int[] ar)
        {
            List<int> list = new List<int>(ar);

            int result = 0;
            while(list.Count > 0)
            {
                var sock1 = list.First();
                list.RemoveAt(0);

                var pairIndex = list.IndexOf(sock1);
                if(pairIndex != -1)
                {
                    result++;
                    list.RemoveAt(pairIndex);
                }
            }
            return result;
        }

        public struct Music
        {
            public int id;
            public int time;
            public string genre;
        }

        // 음악장르 재생 리스트(내가 한거)
        // query 쿼리. 정렬, 해시
        public int[] mySelectMusic(string[] genres, int[] plays)
        {
            List<int> result = new List<int>();

            // genres 음악별 장르
            // plays 음악별 재생시간
            Dictionary<string, int> genreTimes = new Dictionary<string, int>();
            Dictionary<string, List<Music>> genreMusics = new Dictionary<string, List<Music>>();

            for (int i = 0; i < genres.Count(); i++)
            {
                Music music = new Music { id = i, time = plays[i], genre = genres[i] };
                if (genreTimes.ContainsKey(music.genre))
                {
                    genreTimes[music.genre] += plays[i];
                }
                else
                {
                    genreTimes[music.genre] = plays[i];
                }
                if (genreMusics.ContainsKey(music.genre))
                {
                    genreMusics[music.genre].Add(music);
                }
                else
                {
                    genreMusics[music.genre] = new List<Music>();
                    genreMusics[music.genre].Add(music);
                }
            }

            var SortedGenres = genreTimes.ToList();
            SortedGenres.Sort((l, r) =>
            {
                if (l.Value < r.Value)
                {
                    return 1;
                }
                else if (l.Value > r.Value)
                {
                    return -1;
                }
                else
                {
                    return 0;
                }
            });
            foreach (var kv in genreMusics)
            {
                kv.Value.Sort((l, r) =>
                {
                    if (l.time < r.time)
                    {
                        return 1;
                    }
                    else if (l.time > r.time)
                    {
                        return -1;
                    }
                    else
                    {
                        return 0;
                    }
                });
            }

            foreach (var genr in SortedGenres)
            {
                var musics = genreMusics[genr.Key];
                foreach (var music in musics)
                {
                    result.Add(music.id);
                }
            }

            return result.ToArray();
        }

        // 음악장르 재생 리스트(다른 사람거 수정한거)
        // query 쿼리. 정렬, 해시
        public int[] someonesSelectSong(string[] genres, int[] plays)
        {
            List<int> result = new List<int>();

            List<Music> musics = new List<Music>();

            for (int i = 0; i < genres.Count(); i++)
            {
                musics.Add(new Music { id = i, genre = genres[i], time = plays[i] });
            }

            var query = from genre in
                            (from song in musics
                             orderby song.time descending
                             group song by song.genre into g
                             select new
                             {
                                 genre = g.First().genre,
                                 playSum = g.Sum(x => x.time),
                                 musics = g
                             })
                        orderby genre.playSum descending
                        select genre;

            foreach (var hitGenre in query)
            {
                var hitMusics = hitGenre.musics.Take(2);
                foreach (var music in hitMusics)
                {
                    result.Add(music.id);
                }
            }

            return result.ToArray();
        }

        // 정렬하고 k번째 찾기
        public static int[] selectK(int[] array, int[,] commands)
        {
            var result = new List<int>();
            var list = array.ToList();

            for (int n = 0; n < commands.GetLength(0); n++)
            {
                var i = commands[n, 0];
                var j = commands[n, 1];
                var k = commands[n, 2];

                //var subList = list.Where((v, idx) => idx >= i - 1 && idx < j).OrderBy(v => v).ToList(); // 이게 좀 더 느림
                var subList = list.Take(j).Skip(i - 1).ToList(); // 이게 더 빠름
                subList.Sort();

                result.Add(subList[k - 1]);
            }

            return result.ToArray();
        }

        // 가장 큰 숫자 조합 찾기. 정렬
        public static string createMaxNum(int[] numbers)
        {
            Array.Sort(numbers, (l, r) =>
            {
                var str_l = l.ToString();
                var str_r = r.ToString();
                var lr = str_l + str_r;
                var rl = str_r + str_l;

                return rl.CompareTo(lr);
            });

            if (numbers[0] == 0)
            {
                return "0";
            }
            return string.Join("", numbers);
        }

        // 논문 H-Index 구하는거. 정렬
        public static int h_index(int[] citations)
        {
            Array.Sort(citations, (l, r) => l > r ? -1 : 1); // 내림차순. l보다 r이 더 크면 바꾸지 마라(-1)
            // -1이 l, r 순으로
            // 1이 r, l 순으로
            // 0이 l, r 그대로

            int temp = 0;

            for (int i = 0; i < citations.Length; i++)
            {
                int paper_no = i + 1;

                if (citations[i] <= temp)
                {
                    return temp;
                }
                if (citations[i] <= paper_no)
                {
                    return citations[i];
                }
                else
                {
                    temp = paper_no;
                }
            }

            return temp;
        }

        public struct BridgeTruck
        {
            public int truck_weight;
            public int out_time;
        }

        // 자동차 다리 건너는 시간 구하기. 큐
        public static int bridge(int bridge_length, int weight, int[] truck_weights)
        {
            var wait_queue = new Queue<BridgeTruck>();
            var processing_queue = new Queue<BridgeTruck>();

            var current_time = 1;
            var weight_on_bridge = 0;

            foreach (var w in truck_weights)
            {
                wait_queue.Enqueue(new BridgeTruck { truck_weight = w, out_time = 0 });
            }

            while (wait_queue.Count != 0 || processing_queue.Count != 0)
            {
                if (wait_queue.Count == 0 || weight_on_bridge + wait_queue.Peek().truck_weight > weight)
                {
                    current_time = processing_queue.Peek().out_time;
                }
                else
                {
                    var process = wait_queue.Dequeue();
                    process.out_time = current_time + bridge_length;
                    processing_queue.Enqueue(process);
                    weight_on_bridge += process.truck_weight;
                    current_time++;
                }

                if (processing_queue.Count > 0 && current_time >= processing_queue.Peek().out_time)
                {
                    weight_on_bridge -= processing_queue.Dequeue().truck_weight;
                }
            }

            return current_time;
        }
    }
}
